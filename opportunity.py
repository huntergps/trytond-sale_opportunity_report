# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from __future__ import with_statement
from datetime import date
from trytond.model import ModelView, ModelSQL, fields, Workflow
from trytond.transaction import Transaction
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, In
#from trytond.modules.sale_opportunity import SaleOpportunityReportMixin

__all__ = ['Opportunity']

class Opportunity:
    __metaclass__ = PoolMeta
    __name__ = 'sale.opportunity'
    reports = fields.One2Many('sale.opportunity.report', 'opportunity',
        'Reports', context={
            'opportunity_party': Eval('party'),
            'opportunity_party_rel': Eval('company_rel'),
            }, depends=['party'])    
    

