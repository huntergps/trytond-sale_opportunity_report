# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
import datetime

from decimal import Decimal
from trytond.model import ModelSQL, ModelView, Workflow, fields, sequence_ordered
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond import backend
from trytond.pyson import Eval, In, If, Get, Bool
from trytond.modules.company import CompanyReport

from dateutil.relativedelta import relativedelta
from sql import Column, Null, Window, Literal
from sql.aggregate import Sum, Max, Count
from sql.conditionals import Coalesce, Case
from collections import namedtuple


__all__ = ['OpportunityReport','OpportunityReportJobs','OpportunityReportHistorial']

STATES = [
    ('draft', 'Draft'),
    ('confirmed', 'Confirmed'),
]
_STATES_START = {
    'readonly': Eval('state') != 'draft',
    }

_DEPENDS_START = ['state']
_STATES_STOP = {
    'readonly': In(Eval('state'), ['confirmed']),
}

_DEPENDS_STOP = ['state']


def set_dataset(cursor):
    #rdef = namedtuple('dataset', ' '.join([x[0] for x in cursor.description]))
    rdef = namedtuple('dataset', ' '.join([x[0] for x in cursor.description]))
    return map(rdef._make, cursor.fetchall())    



class OpportunityReport(ModelSQL, ModelView):
    'Sale Opportunity Report'
    __name__ = "sale.opportunity.report"

    _rec_name = 'number'
    number = fields.Char('Number', readonly=True, required=False, select=True)
    opportunity = fields.Many2One('sale.opportunity', 'Opportunitie',
        required=True, ondelete='CASCADE')   
    opportunity = fields.Many2One('sale.opportunity', 'Opportunitie',
        required=True, ondelete='CASCADE')     
    subject = fields.Char('Subject')
    salesman = fields.Many2One('company.employee', 'Salesman', required=True,
            states=_STATES_STOP, depends=['state', 'company'],
            domain=[('company', '=', Eval('company'))])
    report_date = fields.Date('Report Date', required=True, select=True,
        states=_STATES_START, depends=_DEPENDS_START)    
    company = fields.Many2One('company.company', 'Company', required=True,
        select=True, states=_STATES_STOP, domain=[
            ('id', If(In('company', Eval('context', {})), '=', '!='),
                Get(Eval('context', {}), 'company', 0)),
            ], depends=_DEPENDS_STOP)        
    refreshments = fields.Char('Refreshments')
    entertaiment = fields.Char('Entertaiment')
    gifts = fields.Char('Corpotate Gifts')
    party = fields.Many2One('party.party', 'Customer', required=True, select=True)   
    company_rel = fields.Many2One('party.party', 'Company Related')
    quality_service = fields.Selection([
            ('vgood', 'Very Good'),
            ('good', 'Good'),
            ('regular', 'Regular'),
            ('bad', 'Bad'),
            ], 'How was the Service', required=True)
    email_updates = fields.Boolean('Are you getting Email updates')
    email = fields.Char('Email for updates')
    hazmat = fields.Boolean('Hazmat')
    reefer = fields.Boolean('Reefer')
    liquor = fields.Boolean('Liquor')
    bonded = fields.Boolean('Bonded')
    triaxle_chassis = fields.Boolean('Triaxle Chassis')
    flat_bend = fields.Boolean('Flat Bend')
    notes = fields.Text('Notes')
    state = fields.Selection(STATES, 'State', required=True)
    jobs = fields.One2Many('sale.opportunity.report.jobs', 'opportunity_report', 'Jobs History', states={
        'readonly': Eval('state') != 'draft',
        },
    depends=['party', 'state'])
    locations = fields.One2Many('sale.opportunity.report.locations', 'opportunity_report', 'Locations', states={
        'readonly': Eval('state') != 'draft',
        },
    depends=['party', 'state'])

    @classmethod
    def __setup__(cls):
        super(OpportunityReport, cls).__setup__()
        cls._order.insert(0, ('report_date', 'DESC'))
        cls._order.insert(1, ('subject', 'DESC'))
        cls._error_messages.update({
                'delete_cancel': ('Sale Opportunity "%s" must be cancelled '
                    'before deletion.'),
                'no_sale_opportunity_report_sequence': ('There is no report sequence '
                    'defined. Please define on in sale configuration.'),
                })
        cls._buttons.update(
                {'get_jobs_history': 
                    {
                    'invisible': Eval('state') != 'draft',
                    }
                })



    def get_rec_name(self, name):
        if self.subject:
            return '[%s] %s' % (self.id, self.subject)
        return self.id

    @classmethod
    def search_rec_name(cls, name, clause):
        return ['OR',
            ('id',) + tuple(clause[1:]),
            ('subject',) + tuple(clause[1:]),
            ]

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_report_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_salesman():
        User = Pool().get('res.user')
        user = User(Transaction().user)
        return user.employee and user.employee.id or None

    @staticmethod
    def default_quality_service():
        return 'good'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @classmethod
    def default_party(cls):
        opportunity_party_id = Transaction().context.get('opportunity_party')
        if opportunity_party_id:
            return opportunity_party_id

    @classmethod
    def default_company_rel(cls):
        opportunity_party_id = Transaction().context.get('opportunity_party_rel')
        if opportunity_party_id:
            return opportunity_party_id

    @fields.depends('party', 'company_rel')
    def on_change_party(self):
        if not self.party:
            return
        if self.party.company:
            self.company_rel = self.party.company.id

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Config = pool.get('sale.configuration')

        config = Config(1)
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            if vals.get('number') is None:
                vals['number'] = Sequence.get_id(
                    config.sale_opportunity_report_sequence.id)
        return super(OpportunityReport, cls).create(vlist)

    @classmethod
    @ModelView.button
    def get_jobs_history(cls,mrecords):
        '''
        Create historial for jobs on the date interval
        '''

        pool = Pool()
        context = Transaction().context
        Jobs = Pool().get('sale.opportunity.report.jobs')
        HistorialLines = pool.get('sale.opportunity.report.historial')

        line = HistorialLines.__table__()
        for mrecord in mrecords:
            columns = [
                line.rolnum.as_('party'),
                line.jobzip.as_('zip'), 
                line.company.as_('company'),                
                Max(line.sdocdt).as_('last_date'),
                (Count(line.id)).as_('total_jobs'),                
                ]
            cursor = Transaction().connection.cursor()
            msql= line.select(*columns,
                where=(line.company == context.get('company'))
                & (line.rolnum == mrecord.party.code),
                 group_by=(line.rolnum, line.jobzip, line.company ))
            print msql
            print mrecord.party.code
            cursor.execute(*msql)

            mrecords = set_dataset(cursor)
            to_create = []
            for r in mrecords:
                to_create.append({
                    'zip':  r.zip,
                    'last_date':  r.last_date,                    
                    'opportunity_report': mrecord.id,
                    'total_jobs': r.total_jobs,
                    })
            print to_create                 
            Jobs.create(to_create) 
            pass



 

class OpportunityReportJobs(sequence_ordered(), ModelSQL, ModelView):
    'Sale Opportunity Report Job'
    __name__ = "sale.opportunity.report.jobs"
    _rec_name = "jobs"
    _states = {
        'readonly': Eval('opportunity_state').in_(
            ['converted', 'won', 'lost', 'cancelled']),
        }
    _depends = ['opportunity_state']
    
    zip = fields.Char('Zip', states=_states, depends=_depends)
    city = fields.Char('City', states=_states, depends=_depends)
    country = fields.Many2One('country.country', 'Country',
        states=_states, depends=_depends)
    subdivision = fields.Many2One("country.subdivision",
        'Subdivision', domain=[
            ('country', '=', Eval('country', -1)),
            ('parent', '=', None),
            ],
        states=_states, depends=['active', 'country'])

    opportunity_report = fields.Many2One('sale.opportunity.report', 'Opportunity Report',
        states={
            'readonly': _states['readonly'] & Bool(Eval('opportunity')),
            },
        depends=_depends)
    opportunity_state = fields.Function(
        fields.Selection(STATES, 'Opportunity State'),
        'on_change_with_opportunity_state')
    last_date = fields.Date('Last Job Date', readonly=True)    
    total_jobs = fields.Float('Total Jobs', required=True,
        digits=(16, Eval('unit_digits', 2)),
        states=_states, depends=['unit_digits'] + _depends)

    del _states, _depends

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)
        super(OpportunityReportJobs, cls).__register__(module_name)

    @classmethod
    def __setup__(cls):
        super(OpportunityReportJobs, cls).__setup__()
        cls._order.insert(0, ('last_date', 'ASC'))
        cls._error_messages.update({
                'create_historial_confirm_report': ('You can not create '
                    'a historial on report "%s" because it is confirmed.'),
                
                })
        



    @fields.depends('opportunity_report', '_parent_opportunity.state')
    def on_change_with_opportunity_state(self, name=None):
        if self.opportunity_report:
            return self.opportunity_report.state
    
    
    @classmethod
    def create(cls, vlist):
        Report = Pool().get('sale.opportunity.report')
        vlist = [x.copy() for x in vlist]
        for vals in vlist:
            if vals.get('opportunity_report'):
                report = Report(vals['opportunity_report'])
                if report.state != 'draft':
                    cls.raise_user_error('create_historial_confirm_report',
                        (report.rec_name,))
        return super(OpportunityReportJobs, cls).create(vlist)    

class OpportunityReportLocations(sequence_ordered(), ModelSQL, ModelView):
    'Sale Opportunity Report Locations'
    __name__ = "sale.opportunity.report.locations"
    _rec_name = "locations"
    _states = {
        'readonly': Eval('opportunity_state').in_(
            ['confirmed',]),
        }
    _depends = ['opportunity_state']

    zip = fields.Char('Zip', states=_states, depends=_depends)
    city = fields.Char('City', states=_states, depends=_depends)
    country = fields.Many2One('country.country', 'Country',
        states=_states, depends=_depends)
    subdivision = fields.Many2One("country.subdivision",
        'Subdivision', domain=[
            ('country', '=', Eval('country', -1)),
            ('parent', '=', None),
            ],
        states=_states, depends=['active', 'country'])

    opportunity_report = fields.Many2One('sale.opportunity.report', 'Opportunity Report',
        states={
            'readonly': _states['readonly'] & Bool(Eval('opportunity')),
            },
        depends=_depends)
    opportunity_state = fields.Function(
        fields.Selection(STATES, 'Opportunity State'),
        'on_change_with_opportunity_state')
    value = fields.Float('Value', required=True,
        digits=(16, Eval('unit_digits', 2)),
        states=_states, depends=['unit_digits'] + _depends)
    target_price = fields.Float('Target Price', required=True,
        digits=(16, Eval('unit_digits', 2)),
        states=_states, depends=['unit_digits'] + _depends)
    our_price = fields.Float('Our Price', required=True,
        digits=(16, Eval('unit_digits', 2)),
        states=_states, depends=['unit_digits'] + _depends)
    space_rate = fields.Float('Space Rate', required=True,
        digits=(16, Eval('unit_digits', 2)),
        states=_states, depends=['unit_digits'] + _depends)
    
    del _states, _depends

    @classmethod
    def __register__(cls, module_name):
        TableHandler = backend.get('TableHandler')
        table = TableHandler(cls, module_name)

        super(OpportunityReportLocations, cls).__register__(module_name)

    _autocomplete_limit = 100

    def _autocomplete_domain(self):
        domain = []
        if self.country:
            domain.append(('country', '=', self.country.id))
        if self.subdivision:
            domain.append(['OR',
                    ('subdivision', 'child_of',
                        [self.subdivision.id], 'parent'),
                    ('subdivision', '=', None),
                    ])
        return domain

    def _autocomplete_search(self, domain, name):
        pool = Pool()
        Zip = pool.get('country.zip')
        if domain:
            records = Zip.search(domain, limit=self._autocomplete_limit)
            if len(records) < self._autocomplete_limit:
                return sorted({getattr(z, name) for z in records})
        return []

    @fields.depends('city', 'country', 'subdivision')
    def autocomplete_zip(self):
        domain = self._autocomplete_domain()
        if self.city:
            domain.append(('city', 'ilike', '%%%s%%' % self.city))
        return self._autocomplete_search(domain, 'zip')

    @fields.depends('zip', 'country', 'subdivision')
    def autocomplete_city(self):
        domain = self._autocomplete_domain()
        if self.zip:
            domain.append(('zip', 'ilike', '%s%%' % self.zip))
        return self._autocomplete_search(domain, 'city')


    @fields.depends('subdivision', 'country')
    def on_change_country(self):
        if (self.subdivision
                and self.subdivision.country != self.country):
            self.subdivision = None

    @fields.depends('opportunity_report', '_parent_opportunity.state')
    def on_change_with_opportunity_state(self, name=None):
        if self.opportunity_report:
            return self.opportunity_report.state



class OpportunityReportHistorial(ModelSQL, ModelView):
    'Sale Opportunity Report Historial'
    __name__ = "sale.opportunity.report.historial"
    _rec_name = "historial"
    company = fields.Many2One('company.company', 'Company', required=True,
        select=True, domain=[
            ('id', If(In('company', Eval('context', {})), '=', '!='),
                Get(Eval('context', {}), 'company', 0)),
            ],)         
    rolnum = fields.Char('Party Related')
    cusnum = fields.Char('Customer')
    ohoins= fields.Char('ohoins')
    refnum= fields.Char('Ref Number')
    sdocdt = fields.Date('Job Date')
    jobzip = fields.Char('Zip')




class ReportReport(CompanyReport):
    __name__ = 'sale.opportunity.report'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(address_with_party=True):
            return super(ReportReport, cls).execute(ids, data)




