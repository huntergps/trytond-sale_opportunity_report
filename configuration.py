#The COPYRIGHT file at the top level of this repository contains the full
#copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval

__all__ = ['SaleConfiguration']



class SaleConfiguration:
    'Sale Configuration'
    __metaclass__ = PoolMeta
    __name__ = 'sale.configuration'
    sale_opportunity_report_sequence = fields.Property(fields.Many2One('ir.sequence',
            'Opportunity Report Sequence', domain=[
                ('company', 'in', [Eval('context', {}).get('company', -1),
                        None]),
                ('code', '=', 'sale.opportunity_report'),
                ], required=True))

