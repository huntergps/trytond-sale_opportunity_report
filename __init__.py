# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from opportunity import Opportunity
from report import OpportunityReport,OpportunityReportJobs, OpportunityReportLocations,ReportReport,OpportunityReportHistorial
from configuration import  SaleConfiguration

def register():
    Pool.register(
        Opportunity,
        SaleConfiguration, 
        OpportunityReport,OpportunityReportJobs, OpportunityReportLocations,OpportunityReportHistorial,
        module='sale_opportunity_report', type_='model')
    Pool.register(
        ReportReport,
        module='sale_opportunity_report', type_='report')
